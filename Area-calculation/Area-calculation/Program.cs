﻿using System;

namespace Area_calculation
{
    class Area
    {
        /// <summary>
        /// Function to calculate the area of square
        /// </summary>
        /// <param name="side">Side of the square</param>
        /// <returns>float value represents area of square</returns>
        static float AreaOfShape(float side)
        {
            return side * side;
        }

        /// <summary>
        /// Function to calculate the area of circle
        /// </summary>
        /// <param name="radius">radius of a circle</param>
        /// <returns>double value represents area of circle</returns>
        static double AreaOfShape(double radius)
        {
            return Math.PI * radius * radius;
        }

        /// <summary>
        /// Function to calculate the area of rectangle
        /// </summary>
        /// <param name="length">length of a rectangle</param>
        /// <param name="breadth">breadth of a rectangle</param>
        /// <returns>float vlue represents area of rectangle</returns>
        static float AreaOfShape(float length, float breadth)
        {
            return length * breadth;
        }
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Enter Side of a Square");
                float side = Convert.ToSingle(Console.ReadLine());
                Console.WriteLine("Area of square " + AreaOfShape(side));
                Console.WriteLine("Enter Radius of a circle");
                double radius = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Area of a Circle " + AreaOfShape(radius));
                Console.WriteLine("Enter length and breadth of rectangle");
                float length = Convert.ToSingle(Console.ReadLine());
                float breadth = Convert.ToSingle(Console.ReadLine());
                Console.WriteLine("Area of a Rectangle " + AreaOfShape(length, breadth));
            }
            catch (FormatException e) {
            Console.WriteLine("Input value not in a correct format");
            }
         }
    }
}
